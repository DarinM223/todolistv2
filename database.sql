CREATE TABLE IF NOT EXISTS users (
    id SERIAL,
    name VARCHAR NOT NULL,
    password VARCHAR NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS lists (
    id SERIAL,
    created_id INTEGER NOT NULL,
    name VARCHAR NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (created_id) REFERENCES users (id)
);

CREATE TABLE IF NOT EXISTS todos (
    id SERIAL,
    list_id INTEGER NOT NULL,
    name VARCHAR NOT NULL,
    cleared BOOLEAN NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (list_id) REFERENCES lists (id)
);
