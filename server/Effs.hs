{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
module Effs where
  
import Common.Types
import Data.Text (Text)
import Optics
import Types

data UserRepo m = UserRepo
  { getUser      :: UserID -> m (Maybe User)
  , getUserLists :: UserID -> m [TodoListID]
  , loginUser    :: Login -> m (Maybe User)
  , createUser   :: CreateUser -> m (Maybe UserID)
  , deleteUser   :: UserID -> m Bool
  }

data TodoListRepo m = TodoListRepo
  { getTodoList    :: TodoListID -> m (Maybe TodoList)
  , createTodoList :: UserID -> Text -> m (Maybe TodoListID)
  , deleteTodoList :: TodoListID -> m Bool
  , addTodo        :: TodoListID -> Text -> m (Maybe TodoID)
  , deleteTodo     :: TodoID -> m Bool
  }

data Effs m = Effs
  { effsUserRepo     :: UserRepo m
  , effsTodoListRepo :: TodoListRepo m
  }
makeFieldLabels ''Effs

type Has s ctx eff = LabelOptic' s A_Lens ctx eff
type HasUserRepo ctx m = Has "userRepo" ctx (UserRepo m)
type HasTodoListRepo ctx m = Has "todoListRepo" ctx (TodoListRepo m)
