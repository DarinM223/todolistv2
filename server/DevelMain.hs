module DevelMain
  ( persistRepl
  , getConfig
  , getEff
  , update
  ) where

import Data.Maybe (fromJust)
import Data.Word (Word32)
import Foreign.Store (newStore, lookupStore, readStore)
import Main
import Query (mkUserRepoPostgres, mkTodoListRepoPostgres)
import Rapid
import Types (Config (..), mkConfig)

-- Persist config and effect values across reloads.
--
-- Example:
-- ```
-- >> import Effs
-- >> :set -XRecordWildCards
-- >> persistRepl
-- Config: Store 0
-- UserRepo: Store 1
-- TodoListRepo: Store 2
-- >> :r
-- >> UserRepo{..} <- getEff 1
-- ```
--
-- Example changing effect:
-- ```
-- >> import Effs
-- >> :set -XRecordWildCards
-- >> persistRepl
-- Config: Store 0
-- UserRepo: Store 1
-- TodoListRepo: Store 2
-- >> :r
-- >> config <- getConfig 0
-- >> writeStore (Store 1) (mkUserRepoPostgres (configPool config))
-- >> UserRepo{..} <- getEff 1
-- ```
persistRepl :: IO ()
persistRepl = do
  config <- mkConfig
  putStr "Config: " >> newStore config >>= print
  putStr "UserRepo: " >>
    newStore (mkUserRepoPostgres (configPool config)) >>= print
  putStr "TodoListRepo: " >>
    newStore (mkTodoListRepoPostgres (configPool config)) >>= print

getConfig :: Word32 -> IO Config
getConfig i = lookupStore i >>= readStore . fromJust

getEff :: Word32 -> IO (f IO)
getEff i = lookupStore i >>= readStore . fromJust

update :: IO ()
update = rapid 0 $ \r -> do
  config <- createRef r "config" mkConfig
  restart r "server" $ startServer 3000 config
