{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
module Types
  ( Password
  , CreateUser (..)
  , Config (..)
  , mkPassword
  , validatePassword
  , mkConfig
  ) where

import Data.Aeson
import Data.ByteString (ByteString)
import Data.Pool
import Data.Text (Text)
import Database.PostgreSQL.Simple.FromField (FieldParser, FromField, fromField)
import Database.PostgreSQL.Simple.ToField (ToField, toField)
import GHC.Generics
import Servant.Auth.Server
  ( JWTSettings
  , CookieSettings
  , defaultCookieSettings
  , defaultJWTSettings
  , generateKey
  )

import qualified Crypto.BCrypt as BCrypt
import qualified Database.PostgreSQL.Simple as P

mapParser :: (a -> b) -> FieldParser a -> FieldParser b
mapParser = fmap . fmap . fmap

newtype Password = Password ByteString
instance ToField Password where
  toField (Password pass) = toField pass
instance FromField Password where
  fromField = mapParser Password fromField

mkPassword :: ByteString -> IO (Maybe Password)
mkPassword pass = fmap Password <$>
  BCrypt.hashPasswordUsingPolicy BCrypt.slowerBcryptHashingPolicy pass

validatePassword :: ByteString -> Password -> Bool
validatePassword pass (Password hashed) =
  BCrypt.validatePassword hashed pass

data CreateUser = CreateUser
  { createName     :: Text
  , createPassword :: Text
  } deriving Generic
instance FromJSON CreateUser

data Config = Config
  { configUsername :: String
  , configPassword :: String
  , configDbname   :: String
  , configJWT      :: JWTSettings
  , configCookie   :: CookieSettings
  , configPool     :: Pool P.Connection
  }

data JsonConfig = JsonConfig
  { configUsername :: String
  , configPassword :: String
  , configDbname   :: String
  }
instance FromJSON JsonConfig where
  parseJSON = withObject "JsonConfig" $ \v ->
    JsonConfig <$> v .: "username" <*> v .: "password" <*> v .: "database"

numPools :: Int
numPools = 4

mkConfig :: IO Config
mkConfig = eitherDecodeFileStrict "./secrets/config.json" >>= \case
  Right JsonConfig{..} -> do
    let connInfo = P.defaultConnectInfo
          { P.connectUser     = configUsername
          , P.connectPassword = configPassword
          , P.connectDatabase = configDbname }
    pool <- createPool (P.connect connInfo) P.close numPools 1 2
    key <- generateKey
    return Config { configJWT    = defaultJWTSettings key
                  , configCookie = defaultCookieSettings
                  , configPool   = pool
                  , .. }
  Left e -> error $ "Error parsing config: " ++ e
