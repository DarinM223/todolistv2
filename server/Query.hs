{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
module Query
  ( mkUserRepoPostgres
  , mkTodoListRepoPostgres
  ) where

import Common.Types
import Control.Monad.Except (guard)
import Control.Monad.Trans
import Control.Monad.Trans.Maybe
import Data.Maybe (listToMaybe)
import Data.Pool
import Data.Text (Text)
import Data.Text.Encoding (encodeUtf8)
import Effs
import Text.RawString.QQ
import Types
import qualified Database.PostgreSQL.Simple as P

mkUserRepoPostgres :: Pool P.Connection -> UserRepo IO
mkUserRepoPostgres pool = UserRepo
  { getUser = withResource pool . getUserDb
  , getUserLists = withResource pool . getUserListsDb
  , loginUser = withResource pool . loginUserDb
  , createUser = withResource pool . createUserDb
  , deleteUser = withResource pool . deleteUserDb
  }

getUserDb :: UserID -> P.Connection -> IO (Maybe User)
getUserDb uid conn = listToMaybe <$>
  P.query conn "SELECT id, name FROM users WHERE id = ?" (P.Only uid)

getUserListsDb :: UserID -> P.Connection -> IO [TodoListID]
getUserListsDb uid conn = P.query conn query (P.Only uid)
 where
  query = [r| SELECT lists.id FROM lists
              INNER JOIN users ON users.id = lists.created_id
              WHERE users.id = ? |]

loginUserDb :: Login -> P.Connection -> IO (Maybe User)
loginUserDb (Login username password) conn = runMaybeT $ do
  (uid, name, password') <- lift getUserFromName >>= MaybeT . pure
  guard $ validatePassword (encodeUtf8 password) password'
  return $ User uid name
 where
  getUserFromName = listToMaybe <$> P.query conn
    "SELECT id, name, password FROM users WHERE name = ?" (P.Only username)

createUserDb :: CreateUser -> P.Connection -> IO (Maybe UserID)
createUserDb (CreateUser username passwordText) conn = runMaybeT $ do
  hashed <- lift (mkPassword $ encodeUtf8 passwordText) >>= MaybeT . pure
  lift (insertUser hashed) >>= MaybeT . pure
 where
  insertUser :: Password -> IO (Maybe UserID)
  insertUser password = listToMaybe <$> P.query conn
    "INSERT INTO users (name, password) VALUES (?, ?) RETURNING id"
    (username, password)

deleteUserDb :: UserID -> P.Connection -> IO Bool
deleteUserDb uid conn = (> 0) <$>
  P.execute conn "DELETE FROM users WHERE id = ?" (P.Only uid)

mkTodoListRepoPostgres :: Pool P.Connection -> TodoListRepo IO
mkTodoListRepoPostgres pool = TodoListRepo
  { getTodoList = withResource pool . getTodoListDb
  , createTodoList = \uid name -> withResource pool $ createTodoListDb uid name
  , deleteTodoList = withResource pool . deleteTodoListDb
  , addTodo = \tid name -> withResource pool $ addTodoDb tid name
  , deleteTodo = withResource pool . deleteTodoDb
  }

getTodoListDb :: TodoListID -> P.Connection -> IO (Maybe TodoList)
getTodoListDb tid conn = do
  list <- listToMaybe
      <$> P.query conn "SELECT id, name FROM lists WHERE id = ?" (P.Only tid)
  todos <- P.query_ conn
    [r| SELECT todos.id, todos.list_id, todos.name, todos.cleared
        FROM todos
        INNER JOIN lists ON lists.id = todos.list_id |]
  return $ fmap (\l -> l { todoListTodos = todos }) list

createTodoListDb :: UserID -> Text -> P.Connection -> IO (Maybe TodoListID)
createTodoListDb uid name conn = listToMaybe <$> P.query conn
  "INSERT INTO lists (created_id, name) VALUES (?, ?) RETURNING id" (uid, name)

deleteTodoListDb :: TodoListID -> P.Connection -> IO Bool
deleteTodoListDb tid conn = (> 0) <$>
  P.execute conn "DELETE FROM lists WHERE id = ?" (P.Only tid)

addTodoDb :: TodoListID -> Text -> P.Connection -> IO (Maybe TodoID)
addTodoDb tid name conn = listToMaybe <$> P.query conn
  "INSERT INTO todos (list_id, name, cleared) VALUES (?, ?, ?) RETURNING id"
  (tid, name, False)

deleteTodoDb :: TodoID -> P.Connection -> IO Bool
deleteTodoDb tid conn = (> 0) <$>
  P.execute conn "DELETE FROM todos WHERE id = ?" (P.Only tid)
