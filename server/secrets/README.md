Store the `config.json` file in here. The file should look like:

```
{
    "username": username,
    "password": password,
    "database": database
}
```
