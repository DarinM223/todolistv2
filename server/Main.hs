{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}
module Main where

import Common.Server
import Common.Types
import Control.Monad.Trans
import Control.Monad.Trans.Maybe
import Data.Maybe (catMaybes)
import Effs
import Optics
import Query
import Servant
import Servant.Auth.Server
import Types
import qualified Network.Wai.Handler.Warp as Warp

userAPIServer
  :: (HasUserRepo ctx IO, HasTodoListRepo ctx IO)
  => ctx -> User -> Server UserAPI
userAPIServer ctx _ = getUserAtID :<|> getTodosAtID
 where
  UserRepo{..} = ctx ^. #userRepo
  TodoListRepo{..} = ctx ^. #todoListRepo
  getUserAtID uid = liftIO (getUser uid) >>= \case
    Just user -> return user
    Nothing   -> throwAll err401
  getTodosAtID uid = liftIO (getUserLists uid) >>=
    fmap catMaybes . traverse (liftIO . getTodoList)

todoAPIServer :: HasTodoListRepo ctx IO => ctx -> User -> Server TodoAPI
todoAPIServer ctx _ = \tid -> liftIO (getTodoList tid) >>= \case
  Just list -> return list
  Nothing   -> throwAll err401
 where TodoListRepo{..} = ctx ^. #todoListRepo

protectedServer
  :: (HasUserRepo ctx IO, HasTodoListRepo ctx IO)
  => ctx -> AuthResult User -> Server Protected
protectedServer ctx (Authenticated user) =
  userAPIServer ctx user :<|> todoAPIServer ctx user
protectedServer _ _ = throwAll err401

unprotectedServer :: HasUserRepo ctx IO => ctx -> Config -> Server Unprotected
unprotectedServer ctx config
  =    pure "hello, world!"
  :<|> checkCreds ctx config
  :<|> serveDirectoryFileServer "/static"

checkCreds
  :: HasUserRepo ctx IO
  => ctx
  -> Config
  -> Login
  -> Handler (Headers '[ Header "Set-Cookie" SetCookie
                       , Header "Set-Cookie" SetCookie ]
                       NoContent)
checkCreds ctx config l = checkCreds' >>= maybe (throwAll err401) pure
 where
  UserRepo{..} = ctx ^. #userRepo
  checkLogin = liftIO . acceptLogin (configCookie config) (configJWT config)
  checkCreds' = runMaybeT $ do
    user <- liftIO (loginUser l) >>= MaybeT . pure
    lift (checkLogin user) >>= lift . \case
      Just applyCookies -> return $ applyCookies NoContent
      Nothing           -> throwAll err401

server
  :: (HasUserRepo ctx IO, HasTodoListRepo ctx IO)
  => ctx -> Config -> Server (API auths)
server ctx config = protectedServer ctx :<|> unprotectedServer ctx config

--
-- Run these commands to set up database:
-- ```
-- sudo -i -u postgres
-- psql
-- create database todolistv2;
-- \c todolistv2
-- \i database.sql
-- \dt
-- \d users
-- \d lists
-- \d todos
-- ```
--

startServer :: Int -> Config -> IO ()
startServer port config = do
  let effs    = Effs (mkUserRepoPostgres (configPool config))
                     (mkTodoListRepoPostgres (configPool config))
      server' = server effs config
      context = configCookie config :. configJWT config :. EmptyContext
  Warp.run port $ serveWithContext (Proxy @(API '[JWT])) context server'

main :: IO ()
main = mkConfig >>= startServer 3000
