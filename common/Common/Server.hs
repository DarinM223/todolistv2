{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
module Common.Server where

import Common.Types
import Data.Text (Text)
import Servant.API
import Servant.Auth.Server

-- GET /users/:id       -- Get user info at id
-- GET /users/:id/todos -- Get user's todos
-- GET /todos/:id       -- Get todolist at id
-- POST /login          -- Logs in user
-- GET /hello           -- Sample endpoint to check if connected

type Protected
  =    "users" :> UserAPI
  :<|> "todos" :> TodoAPI

type UserAPI
  =    Capture "id" UserID :> Get '[JSON] User
  :<|> Capture "id" UserID :> "todos" :> Get '[JSON] [TodoList]

type TodoAPI = Capture "id" TodoListID :> Get '[JSON] TodoList

type Unprotected
  =    "hello" :> Get '[PlainText] Text
  :<|> "login"
         :> ReqBody '[JSON] Login
         :> PostNoContent '[JSON]
            (Headers '[ Header "Set-Cookie" SetCookie
                      , Header "Set-Cookie" SetCookie ]
                      NoContent)
  :<|> Raw

type API auths = (Auth auths User :> Protected) :<|> Unprotected
