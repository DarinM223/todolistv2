{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
module Common.Types
  ( UserID
  , TodoListID
  , TodoID
  , Login (..)
  , User (..)
  , TodoList (..)
  , Todo (..)
  , userID
  , todoListID
  , todoID
  ) where

import Data.Aeson
import Data.Text (Text)
import Database.PostgreSQL.Simple.FromField (FieldParser, FromField, fromField)
import Database.PostgreSQL.Simple.FromRow (FromRow, field, fromRow)
import Database.PostgreSQL.Simple.ToField (ToField, toField)
import Database.PostgreSQL.Simple.ToRow (ToRow, toRow)
import GHC.Generics
import Servant.Auth.Server (FromJWT, ToJWT)
import Web.HttpApiData

mapParser :: (a -> b) -> FieldParser a -> FieldParser b
mapParser = fmap . fmap . fmap

newtype UserID = UserID Int
  deriving (Show, Generic, FromRow)
instance ToField UserID where
  toField (UserID uid) = toField uid
instance FromField UserID where
  fromField = mapParser UserID fromField
instance FromHttpApiData UserID where
  parseUrlPiece = fmap UserID . parseUrlPiece
instance ToHttpApiData UserID where
  toUrlPiece (UserID uid) = toUrlPiece uid
instance ToJSON UserID
instance FromJSON UserID

userID :: Int -> UserID
userID = UserID

newtype TodoListID = TodoListID Int
  deriving (Show, Generic)
instance ToField TodoListID where
  toField (TodoListID tid) = toField tid
instance FromField TodoListID where
  fromField = mapParser TodoListID fromField
instance FromRow TodoListID where
  fromRow = TodoListID <$> field
instance FromHttpApiData TodoListID where
  parseUrlPiece = fmap TodoListID . parseUrlPiece
instance ToHttpApiData TodoListID where
  toUrlPiece (TodoListID tid) = toUrlPiece tid
instance ToJSON TodoListID
instance FromJSON TodoListID

todoListID :: Int -> TodoListID
todoListID = TodoListID

newtype TodoID = TodoID Int
  deriving (Show, Generic, FromRow)
instance ToField TodoID where
  toField (TodoID tid) = toField tid
instance FromField TodoID where
  fromField = mapParser TodoID fromField
instance ToJSON TodoID
instance FromJSON TodoID

todoID :: Int -> TodoID
todoID = TodoID

data Login = Login
  { loginName     :: Text
  , loginPassword :: Text
  } deriving Generic
instance FromJSON Login
instance ToJSON Login

data User = User
  { userId   :: UserID
  , userName :: Text
  } deriving (Show, Generic, FromRow, ToRow)
instance ToJSON User
instance FromJSON User
instance ToJWT User
instance FromJWT User

data TodoList = TodoList
  { todoListId    :: TodoListID
  , todoListName  :: Text
  , todoListTodos :: [Todo]
  } deriving (Show, Generic)
instance ToRow TodoList where
  toRow l = [toField (todoListId l), toField (todoListName l)]
instance FromRow TodoList where
  fromRow = TodoList <$> field <*> field <*> pure []
instance ToJSON TodoList
instance FromJSON TodoList

data Todo = Todo
  { todoId       :: TodoID
  , todoParentId :: TodoListID
  , todoName     :: Text
  , todoCleared  :: Bool
  } deriving (Show, Generic, FromRow)
instance ToJSON Todo
instance FromJSON Todo
