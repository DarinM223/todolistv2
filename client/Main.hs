{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeApplications #-}
module Main where

import Common.Server (API)
import Control.Lens ((^.))
import Data.Proxy (Proxy (Proxy))
import Language.Javascript.JSaddle
import Servant.Auth.Client ()
import Servant.Auth.Server (JWT)
import Servant.API
import Servant.Client.JSaddle
import qualified Language.Javascript.JSaddle.Warp as JSAddle

import Network.HTTP.Client (defaultManagerSettings, newManager)
import Network.HTTP.Proxy
import Network.WebSockets (defaultConnectionOptions)
import qualified Data.ByteString.Char8 as C
import qualified Network.Wai.Handler.Warp as Warp

run :: Int -> Int -> JSM () -> IO ()
run serverPort proxyPort0 f = do
  proxyApp <- httpProxyApp proxySettings <$> newManager defaultManagerSettings
  JSAddle.jsaddleWithAppOr defaultConnectionOptions (f >> syncPoint) proxyApp
    >>= Warp.runSettings (Warp.setPort proxyPort0 Warp.defaultSettings)
 where
  proxySettings = defaultProxySettings
    { proxyPort            = proxyPort0
    , proxyRequestModifier = \req -> pure $ Right req { requestPath = path req }
    }
  path req = mconcat
    [C.pack "http://localhost:", C.pack (show serverPort), requestPath req]

main :: IO ()
main = do
  let _ :<|> (getHello :<|> _login :<|> _raw) = client (Proxy @(API '[JWT]))
  clientEnv <- mkClientEnv <$> parseBaseUrl "localhost:3709"
  run 3000 3709 $ do
    doc <- jsg "document"
    Right text <- liftJSM $ runClientM getHello clientEnv
    doc ^. js "body" . jss "innerHTML" (mconcat ["<h1>", show text, "</h1>"])
    syncPoint

    return ()
